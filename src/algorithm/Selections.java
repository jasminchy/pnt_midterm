//package algorithm;
//
//import java.util.Random;
//
//public class Selections {
//
//
//    public static void selectionSort(int[] arr){
//        for (int i = 0; i < arr.length - 1; i++)
//        {
//            int index = i;
//            for (int j = i + 1; j < arr.length; j++){
//                if (arr[j] < arr[index]){
//                    index = j;//searching for lowest index
//                }
//            }
//            int smallerNumber = arr[index];
//            arr[index] = arr[i];
//            arr[i] = smallerNumber;
//        }
//    }
//
//    public static void main(String[] args) {
//        int[] num = new int[100];
//
//        Sort algo = new Sort();
//        algo.selectionSort(num);
//
//        long selectionSortExecutionTime = algo.executionTime;
//        System.out.println("Total Execution Time of "+ num.length + " numbers in Selection Sort take: " + selectionSortExecutionTime + " milli sec");
//
//
//        for(String st:numbers){
//            System.out.println(st);
//        }
//
////        for(int i:num){
////            System.out.print(i+" ");
////        }
//
//        int n = num.length;
//        randomize (num, n);
//    }
//
//    public static void randomize( int arr[], int n)
//    {
//        Random r = new Random();
//        // Start from the last element and swap one by one. We don't
//        // need to run for the first element that's why i > 0
//        for (int i = n-1; i > 0; i--) {
//            int j = r.nextInt(i);
//            int temp = arr[i];
//            arr[i] = arr[j];
//            arr[j] = temp;
//        }
//    }
//}
